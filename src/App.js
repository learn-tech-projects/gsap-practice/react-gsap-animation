import React, {useRef, useEffect} from 'react';

// Scss
import './App.scss';

import {
	TweenMax,
	TimelineLite,
	Power3,
} from 'gsap'


// Assets
import arrow from './img/arrow-right.svg'
import imgGirl from './img/girl.webp'
import imgBoy from './img/boy.webp'

function App() {
	let app =  useRef(null);
	let images = useRef(null)

	let content = useRef(null)

	let tl = new TimelineLite({delay:.8});

	useEffect(()=>{
		// IMAGES VARS
		const girlImage = images.firstElementChild;
		const boyImage = images.lastElementChild;

		// CONTENT VARS
		const headLineFirst = content.children[0].children[0]
		const headLineSecond = content.children[0].children[1] // Also can write as headLineFirst.nextSibling
		const headLineThird = content.children[0].children[2]

		const contentP = content.children[1]

		const contentButton = content.children[2]

		TweenMax.to(app, 0, {
			css:{
				visibility:'visible'
			}
		})

		// Images Animation
		tl
			.from(girlImage, 1.2, {
				y:1280,
				ease:Power3.easeOut
			},'Cool kids')
			.from(girlImage.firstElementChild, 2, {
				scale:1.6,
				ease:Power3.easeOut
			}, .2)
			.from(boyImage, 1.2, {
				y:1280,
				ease:Power3.easeOut
			}, .2)
			.from(boyImage.firstElementChild, 2, {
				scale:1.6,
				ease:Power3.easeOut
			}, .2)

		// Content animation
		tl
			.staggerFrom([
				headLineFirst.children,
				headLineSecond.children,
				headLineThird.children,
			],1 ,{
				y:44,
				ease:Power3.easeOut,
				delay:.8
			}, .15, 'Cool kids') // Если задать дополнительным параметром схожие имена, то анимации будут запускаться в один момент. Здесь они запускаются в один момент но за счет того, что у второго есть delay, то он будет запускаться чуть позже и все выглядит действительно хорошо  
			.from(contentP, 1, {
				y:20,
				opacity:0,
				ease: Power3.easeOut
			}, 1.4)
			.from(contentButton, 1, {
				y:20,
				opacity:0,
				ease: Power3.easeOut
			}, 1.6)

		console.log(content)
	}, [tl])	// Тут дописывается дополнительный параметр по которому useEffect не будет работать, пока не прогрузился timeline

  return (
    <div className="hero" ref={el => app = el}>
    	<div className="container">
        	<div className="hero-inner">



				<div className="hero-content">
					
					<div className="hero-content-inner" ref={el => content = el}>
						<h1>

							<div className="hero-content-line">
								<div className="hero-content-line-inner">Relivieng the burden</div>
							</div>

							<div className="hero-content-line">
								<div className="hero-content-line-inner">of desease caused</div>
							</div>

							<div className="hero-content-line">
								<div className="hero-content-line-inner">by behavior.</div>
							</div>

						</h1>

						<p>Dolor eiusmod esse voluptate do fugiat ullamco magna occaecat est nostrud eiusmod laborum fugiat labore.</p>
						
						<div className="btn-row">
							<button className="explore-button">
								Explore

								<div className="arrow-icon">
									<img src={arrow} alt=""/>
								</div>	
							</button>	
						</div>

					</div>
				</div>

				
				
				<div className="hero-images">
					<div className="hero-images-inner" ref={el => images = el}>

						<div className="hero-image girl">
							<img src={imgGirl} alt=""/>
						</div>

						<div className="hero-image boy">
							<img src={imgBoy} alt=""/>
						</div>

					</div>
				</div>



        	</div>
    	</div>
      
    </div>
  );
}

export default App;
